<?php

namespace App\Http\Controllers;

use App\HealthUnitTb;
use Illuminate\Http\Request;

class HealthUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('forms.healthUnitTbForm', ['page_title' => 'Health Unit TB Form']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('forms.healthUnitTbForm', ['page_title' => 'Health Unit TB Form']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $formData = new HealthUnitTb();

        $formData->unit_tb_number = $request->unit_tb_number;
        $formData->hsb_tb_no = $request->hsb_tb_no;
        $formData->district_tb_number = $request->district_tb_number;
        $formData->contact_phone_number = $request->contact_phone_number;
        $formData->next_of_kin_contact_number = $request->next_of_kin_contact_number;
        $formData->next_of_kin_relationship = $request->next_of_kin_relationship;
        $formData->is_patient_health_worker = $request->is_patient_health_worker;
        $formData->cadre_of_health_worker = $request->cadre_of_health_worker;
        $formData->date_treatment_started = $request->date_treatment_started;
        $formData->type_of_patient = $request->type_of_patient;

        $formData->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
