<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHealthUnitTbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_unit_tbs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unit_tb_number')->unique();
            $table->string('hsb_tb_no')->unique();
            $table->string('district_tb_number')->unique();
            $table->string('contact_phone_number' , 10)->unique();
            $table->string('next_of_kin_contact_number' , 10);
            $table->string('next_of_kin_relationship');
            $table->string('is_patient_health_worker' , 3);
            $table->string('cadre_of_health_worker');
            $table->date('date_treatment_started');
            $table->string('type_of_patient');
            $table->string('regimen');
            $table->string('specify_regimen');
            $table->string('transfer_in_from')->nullable();

            $table->timestamps();

            // table uses SoftDeletes
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_unit_tbs');
    }
}
