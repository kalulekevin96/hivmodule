@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header text-center">
        HMIS 096A Health Unit TB Form
    </div>

    <div class="card-body">
        <form action="/form/store" method="POST" class="form">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="unit_tb_number"> Unit TB Number</label>
                        <input type="text" class="form-control" name="unit_tb_number">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="hsb_tb_number"> HSD TB No. </label>
                        <input type="text" class="form-control" name="hsb_tb_number">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="district_tb_number"> District TB Number</label>
                        <input type="text" class="form-control" name="district_tb_number">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="contact_phone_number"> Contact Phone Number </label>
                        <input type="text" class="form-control" name="contact_phone_number">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="next_of_kin_contact_number"> Contact Phone Number
                            <small class="form-text"> (Next Of Kin) </small>
                        </label>
                        <input type="text" class="form-control" name="next_of_kin_contact_number">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="next_of_kin_relationship"> Relationship of Contact Person </label>
                        <input type="text" class="form-control" name="next_of_kin_relationship">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="contact_phone_number"> Is Patient A Health Worker ? </label>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-control-label" for="patient_worker_yes">
                                    <input type="radio" name="is_patient_health_worker" value="yes" class="d-inline">
                                    Yes
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="form-control-label" for="patient_worker_no">
                                    <input type="radio" name="is_patient_health_worker" value="no" class="d-inline">
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="next_of_kin_contact_number"> Cadre Of Health Worker
                        </label>
                        <select name="cadre_of_health_worker" id="cadre_of_health_worker" class="form-control">
                            <option value=""> Select Option </option>
                        </select>

                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="date_treatment_started"> Date treatment started </label>
                        <input type="date" name="date_treatment_started" class="form-control" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="regimen"> Regimen
                        </label>
                        <select name="regimen" id="regimen" class="form-control">
                            <option value=""> Select Option </option>
                        </select>

                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="type_of_patient"> Type of patient
                        </label>
                        <select name="type_of_patient" id="type_of_patient" class="form-control">
                            <option value="New"> New </option>
                            <option value="Old"> Old </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label" for="specify_regimen"> Specify Regimen
                        </label>
                        <select name="specify_regimen" id="specify_regimen" class="form-control">
                            <option value=""> Select Option </option>
                        </select>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 offset-lg-4">
                    <div class="form-group">
                        <label class="form-control-label" for="transfer_in_from"> Transfer In From
                        </label>
                        <input type="text" name="transfer_in_from" class="form-control" />
                    </div>
                </div>
                <div class="col-md-2 offset-lg-2">
                    <input type="submit" class="btn btn-md btn-primary">
                </div>
            </div>
    </div>
</div>

@endsection